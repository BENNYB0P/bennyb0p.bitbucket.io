var classSC__enotor_1_1MotorDriver =
[
    [ "__init__", "classSC__enotor_1_1MotorDriver.html#a12c06041f6dc7a5b07f3f0870571eb79", null ],
    [ "AutoDuty", "classSC__enotor_1_1MotorDriver.html#a58ae21416d16a3e7a9b3d9abc34e84ed", null ],
    [ "Brake", "classSC__enotor_1_1MotorDriver.html#a6529bfa93dd9710a5cab376ae292dd83", null ],
    [ "Coast", "classSC__enotor_1_1MotorDriver.html#ae4005fd24be0ad0a075d27d4e954b6f9", null ],
    [ "Disable", "classSC__enotor_1_1MotorDriver.html#ae75a0adc30e44faff579dc7729b4c39c", null ],
    [ "Enable", "classSC__enotor_1_1MotorDriver.html#a1381933590e7838e71923d52dc165838", null ],
    [ "FaultDetected", "classSC__enotor_1_1MotorDriver.html#a0a0b8e0caf61a982025e66a8d9d6e347", null ],
    [ "Forward", "classSC__enotor_1_1MotorDriver.html#a1cc1e5908b66d9356f8cca318d035066", null ],
    [ "Reverse", "classSC__enotor_1_1MotorDriver.html#a163acac82219b489cc388650c04fe424", null ],
    [ "ch1", "classSC__enotor_1_1MotorDriver.html#a6c931c0a2dda7b8d30cc0a88c20cf08e", null ],
    [ "ch2", "classSC__enotor_1_1MotorDriver.html#ab18538acfba3b58225848e17cc1cd5f8", null ],
    [ "IN1", "classSC__enotor_1_1MotorDriver.html#abd56ed87306d0a36771f0f4869a02eda", null ],
    [ "IN1_pin", "classSC__enotor_1_1MotorDriver.html#aca1adb6c9630b22e0c2ed55ad7628e6c", null ],
    [ "IN2", "classSC__enotor_1_1MotorDriver.html#aa818729067386b459d9946020a213926", null ],
    [ "IN2_pin", "classSC__enotor_1_1MotorDriver.html#a3a4c255a5df2e80c5d1404e5d834b755", null ],
    [ "myFAULT", "classSC__enotor_1_1MotorDriver.html#a134163bdf040b6d7ca7aeabbb6f83871", null ],
    [ "myTimer", "classSC__enotor_1_1MotorDriver.html#a89e00626d03620401c3d9a112d871fcf", null ],
    [ "nSLEEP_pin", "classSC__enotor_1_1MotorDriver.html#a23668e5886a0945e5bd7c8126169ef4e", null ],
    [ "nSLEEPchannel", "classSC__enotor_1_1MotorDriver.html#a6178ae4d996ac4f4166eeccb550e6075", null ],
    [ "nTimer", "classSC__enotor_1_1MotorDriver.html#a8efb8f9ca3cbc94499f083d94cee8462", null ]
];