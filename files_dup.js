var files_dup =
[
    [ "balance_ball.py", "balance__ball_8py.html", "balance__ball_8py" ],
    [ "blu.py", "blu_8py.html", "blu_8py" ],
    [ "Change_Calculator.py", "Change__Calculator_8py.html", "Change__Calculator_8py" ],
    [ "Elevator.py", "Elevator_8py.html", "Elevator_8py" ],
    [ "Fibonacci_Function.py", "Fibonacci__Function_8py.html", "Fibonacci__Function_8py" ],
    [ "FSM_encode.py", "FSM__encode_8py.html", "FSM__encode_8py" ],
    [ "FSM_encode_L7.py", "FSM__encode__L7_8py.html", "FSM__encode__L7_8py" ],
    [ "fullscript_encode.py", "fullscript__encode_8py.html", "fullscript__encode_8py" ],
    [ "main_405_L2.py", "main__405__L2_8py.html", "main__405__L2_8py" ],
    [ "main_405_L3.py", "main__405__L3_8py.html", "main__405__L3_8py" ],
    [ "main_405_L4.py", "main__405__L4_8py.html", "main__405__L4_8py" ],
    [ "main_L2.py", "main__L2_8py.html", "main__L2_8py" ],
    [ "main_L4.py", "main__L4_8py.html", "main__L4_8py" ],
    [ "main_L6.py", "main__L6_8py.html", "main__L6_8py" ],
    [ "main_L7.py", "main__L7_8py.html", "main__L7_8py" ],
    [ "MCP9808.py", "MCP9808_8py.html", "MCP9808_8py" ],
    [ "moset.py", "moset_8py.html", [
      [ "MotorDriver", "classmoset_1_1MotorDriver.html", "classmoset_1_1MotorDriver" ]
    ] ],
    [ "moset_L7.py", "moset__L7_8py.html", [
      [ "MotorDriver", "classmoset__L7_1_1MotorDriver.html", "classmoset__L7_1_1MotorDriver" ]
    ] ],
    [ "SC_blink.py", "SC__blink_8py.html", "SC__blink_8py" ],
    [ "SC_closedloop.py", "SC__closedloop_8py.html", [
      [ "ClosedLoop", "classSC__closedloop_1_1ClosedLoop.html", "classSC__closedloop_1_1ClosedLoop" ]
    ] ],
    [ "SC_closedloop_L7.py", "SC__closedloop__L7_8py.html", [
      [ "ClosedLoop", "classSC__closedloop__L7_1_1ClosedLoop.html", "classSC__closedloop__L7_1_1ClosedLoop" ]
    ] ],
    [ "SC_encode.py", "SC__encode_8py.html", "SC__encode_8py" ],
    [ "SC_enotor.py", "SC__enotor_8py.html", "SC__enotor_8py" ],
    [ "SC_TouchScreen.py", "SC__TouchScreen_8py.html", "SC__TouchScreen_8py" ],
    [ "Spyder_UserInterface.py", "Spyder__UserInterface_8py.html", "Spyder__UserInterface_8py" ],
    [ "Spyderspace.py", "Spyderspace_8py.html", "Spyderspace_8py" ],
    [ "Spyderspace_405.py", "Spyderspace__405_8py.html", "Spyderspace__405_8py" ],
    [ "Spyderspace_L7.py", "Spyderspace__L7_8py.html", "Spyderspace__L7_8py" ],
    [ "VendingMachine.py", "VendingMachine_8py.html", "VendingMachine_8py" ]
];