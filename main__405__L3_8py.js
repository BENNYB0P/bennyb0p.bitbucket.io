var main__405__L3_8py =
[
    [ "userButtonPressed", "main__405__L3_8py.html#a149f373f87a087ae4ea31f5b6363e466", null ],
    [ "curr_time", "main__405__L3_8py.html#a934c4414cfbada33d40f66eaeb093e7a", null ],
    [ "curr_voltage", "main__405__L3_8py.html#a03353bf9c6792b0409ea08bd0fb1fd20", null ],
    [ "data1", "main__405__L3_8py.html#a8489340538d7254f117bcb72880a242e", null ],
    [ "data2", "main__405__L3_8py.html#a4a8c4f4959f91f04710c13c277cc62cd", null ],
    [ "lastInput", "main__405__L3_8py.html#a1234b30ec393afee3943e4735a48ce0d", null ],
    [ "myADC", "main__405__L3_8py.html#a395d62945020584747df14bd269cc82f", null ],
    [ "myButton", "main__405__L3_8py.html#a35bb54d0bfe679a68c818ccf8d79a76e", null ],
    [ "myLED", "main__405__L3_8py.html#a2ce616f5e1315b08bec9a91278ed4b5e", null ],
    [ "myuart", "main__405__L3_8py.html#a262a37cff1a771ae4cde4690b42d737c", null ],
    [ "newInput", "main__405__L3_8py.html#a28ffdf04ac184c86e53a21fc8a9737c8", null ],
    [ "PC_wants_Data", "main__405__L3_8py.html#a4e044a9e0e72bf761d346c5e8cfc978c", null ],
    [ "rawinput", "main__405__L3_8py.html#aaf9b77a5a25e72e5eb4ba7584d567957", null ],
    [ "Rec_Resolution", "main__405__L3_8py.html#a11d1aae073195fe48625acf930d3a330", null ],
    [ "Rec_Status", "main__405__L3_8py.html#a0cdd7ae1240692659d332ac78d85de67", null ],
    [ "rec_t_last", "main__405__L3_8py.html#ac1f00f13b95749d840c2357a520326e6", null ],
    [ "target", "main__405__L3_8py.html#a85e5e4b18db71cc4e07e1bbc5d1e62db", null ]
];