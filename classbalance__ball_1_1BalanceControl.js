var classbalance__ball_1_1BalanceControl =
[
    [ "__init__", "classbalance__ball_1_1BalanceControl.html#a6d6f6d4e382a8c252d8116883ee9851d", null ],
    [ "run", "classbalance__ball_1_1BalanceControl.html#af04c46102a646ed9bdadc3a023a5ecba", null ],
    [ "angular_pos", "classbalance__ball_1_1BalanceControl.html#a09c9c96cd56f063f3b80a303dfe43a37", null ],
    [ "gain_ball_1", "classbalance__ball_1_1BalanceControl.html#a056360753bf51ff5d6323960b3d22777", null ],
    [ "gain_ball_2", "classbalance__ball_1_1BalanceControl.html#a8f9e75758ba91d7f65b7194807473ac3", null ],
    [ "gain_no_ball_1", "classbalance__ball_1_1BalanceControl.html#acdd69acedf4afe1870f4a90fbae333f3", null ],
    [ "gain_no_ball_2", "classbalance__ball_1_1BalanceControl.html#a8fc8de1e1cb39275d10cf4065b1cf3ce", null ],
    [ "myIMU", "classbalance__ball_1_1BalanceControl.html#ab664827502705032795fa526bade8f4a", null ],
    [ "myMotor1", "classbalance__ball_1_1BalanceControl.html#a7438d49917740e954be217b04034b6be", null ],
    [ "myMotor2", "classbalance__ball_1_1BalanceControl.html#ab6d95106357f0f894aa4a8da283eff18", null ],
    [ "myScreen", "classbalance__ball_1_1BalanceControl.html#abe03fb1d398d1ce5f909299826a40f7f", null ],
    [ "pos", "classbalance__ball_1_1BalanceControl.html#ab1e72777b256e7530e50351b5a0e0d77", null ],
    [ "S0_init", "classbalance__ball_1_1BalanceControl.html#a7fbea6e41ff812cd24c716dd0acc8402", null ],
    [ "S1_control", "classbalance__ball_1_1BalanceControl.html#a8ded99fb9e6073fede470ec374b2881f", null ],
    [ "start_time", "classbalance__ball_1_1BalanceControl.html#af3c48d8c76e43b0b40d1adedc3763d44", null ],
    [ "state", "classbalance__ball_1_1BalanceControl.html#a7113d27cfb21ce90dd9686c44f15d44e", null ],
    [ "torque2pwm", "classbalance__ball_1_1BalanceControl.html#a732c5969d0c39ff0c5544cebc1a8eed0", null ]
];