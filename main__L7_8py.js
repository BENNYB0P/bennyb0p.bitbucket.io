var main__L7_8py =
[
    [ "cmd", "main__L7_8py.html#a10d3be6a04f37280e381e8eee361a00d", null ],
    [ "count_blanks", "main__L7_8py.html#a28cef2567a02b05143fb60b2e737e98b", null ],
    [ "curr_pos", "main__L7_8py.html#abf21c1612125be104a447b4df09696b1", null ],
    [ "data1", "main__L7_8py.html#a41ab9fa299e235864e7825081f1ef54d", null ],
    [ "data2", "main__L7_8py.html#afe66ce2749716112a0b7d514740389eb", null ],
    [ "DesiredPosition", "main__L7_8py.html#a7ce8eea1abf367012acfbeaf1324aa8b", null ],
    [ "DesiredSpeed", "main__L7_8py.html#a312e9629e9e359de9850e3e8e0bf9ab8", null ],
    [ "inputPieces", "main__L7_8py.html#aa26775fe246a00e4c3a91cf0e9ada59e", null ],
    [ "Ki", "main__L7_8py.html#af78f736fdcf7664ec67ae6e033d0062a", null ],
    [ "Kp", "main__L7_8py.html#a8b1ea8c30cb46292da77b4bd8faeda17", null ],
    [ "lastInput", "main__L7_8py.html#a99d5e64bd463964a3d95b6d26fc07e3b", null ],
    [ "myController1", "main__L7_8py.html#aec82560e317c6e63889b788cba193d06", null ],
    [ "myEncoder1", "main__L7_8py.html#ab5b953f7c8d256322c295990dad09f8e", null ],
    [ "myMotor1", "main__L7_8py.html#a49fe2cf4f6a918eb9d2ce49375e5f1ca", null ],
    [ "myuart", "main__L7_8py.html#abfa2f1c4661f999e4ee451407c6eafba", null ],
    [ "newInput", "main__L7_8py.html#a59059cec0c3d7f0cfd5da49bebdd6c39", null ],
    [ "P_running", "main__L7_8py.html#af69880a558883550065de5dac64451fe", null ],
    [ "rawinput", "main__L7_8py.html#a5131eb191788953cc25ab6ac834f01f9", null ],
    [ "recording", "main__L7_8py.html#a7985f6aec3a2e5b205e548b56a6c4cc7", null ],
    [ "Syncing", "main__L7_8py.html#a6dd39967a72a55a581cafcd7d72df121", null ],
    [ "target", "main__L7_8py.html#a67775d5342fa1c942dea9b035f7840a5", null ]
];