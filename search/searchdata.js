var indexSectionsWithContent =
{
  0: "_abcefghijlmnprstuv",
  1: "bcelmptu",
  2: "_befhlt",
  3: "bcefmsv",
  4: "_acfgijlprstu",
  5: "abcghilmnprst",
  6: "mt"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Pages"
};

