var annotated_dup =
[
    [ "balance_ball", null, [
      [ "BalanceControl", "classbalance__ball_1_1BalanceControl.html", "classbalance__ball_1_1BalanceControl" ]
    ] ],
    [ "blu", null, [
      [ "Bluetool", "classblu_1_1Bluetool.html", "classblu_1_1Bluetool" ]
    ] ],
    [ "Elevator", "namespaceElevator.html", "namespaceElevator" ],
    [ "FSM_encode", null, [
      [ "EncoderDriver", "classFSM__encode_1_1EncoderDriver.html", "classFSM__encode_1_1EncoderDriver" ]
    ] ],
    [ "FSM_encode_L7", null, [
      [ "EncoderDriver", "classFSM__encode__L7_1_1EncoderDriver.html", "classFSM__encode__L7_1_1EncoderDriver" ]
    ] ],
    [ "fullscript_encode", null, [
      [ "EncoderDriver", "classfullscript__encode_1_1EncoderDriver.html", "classfullscript__encode_1_1EncoderDriver" ],
      [ "UIrequest", "classfullscript__encode_1_1UIrequest.html", "classfullscript__encode_1_1UIrequest" ]
    ] ],
    [ "MCP9808", null, [
      [ "MCP9808", "classMCP9808_1_1MCP9808.html", "classMCP9808_1_1MCP9808" ]
    ] ],
    [ "moset", null, [
      [ "MotorDriver", "classmoset_1_1MotorDriver.html", "classmoset_1_1MotorDriver" ]
    ] ],
    [ "moset_L7", null, [
      [ "MotorDriver", "classmoset__L7_1_1MotorDriver.html", "classmoset__L7_1_1MotorDriver" ]
    ] ],
    [ "SC_blink", null, [
      [ "LED_Driver", "classSC__blink_1_1LED__Driver.html", "classSC__blink_1_1LED__Driver" ],
      [ "TaskBlink", "classSC__blink_1_1TaskBlink.html", "classSC__blink_1_1TaskBlink" ]
    ] ],
    [ "SC_closedloop", null, [
      [ "ClosedLoop", "classSC__closedloop_1_1ClosedLoop.html", "classSC__closedloop_1_1ClosedLoop" ]
    ] ],
    [ "SC_closedloop_L7", null, [
      [ "ClosedLoop", "classSC__closedloop__L7_1_1ClosedLoop.html", "classSC__closedloop__L7_1_1ClosedLoop" ]
    ] ],
    [ "SC_encode", null, [
      [ "EncoderDriver", "classSC__encode_1_1EncoderDriver.html", "classSC__encode_1_1EncoderDriver" ]
    ] ],
    [ "SC_enotor", null, [
      [ "EncoderDriver", "classSC__enotor_1_1EncoderDriver.html", "classSC__enotor_1_1EncoderDriver" ],
      [ "MotorDriver", "classSC__enotor_1_1MotorDriver.html", "classSC__enotor_1_1MotorDriver" ]
    ] ],
    [ "SC_TouchScreen", null, [
      [ "TouchScreen", "classSC__TouchScreen_1_1TouchScreen.html", "classSC__TouchScreen_1_1TouchScreen" ]
    ] ],
    [ "Spyder_UserInterface", null, [
      [ "UIrequest", "classSpyder__UserInterface_1_1UIrequest.html", "classSpyder__UserInterface_1_1UIrequest" ]
    ] ],
    [ "Spyderspace", null, [
      [ "UIrequest", "classSpyderspace_1_1UIrequest.html", "classSpyderspace_1_1UIrequest" ]
    ] ],
    [ "Spyderspace_405", null, [
      [ "UIrequest", "classSpyderspace__405_1_1UIrequest.html", "classSpyderspace__405_1_1UIrequest" ]
    ] ],
    [ "Spyderspace_L7", null, [
      [ "UIrequest", "classSpyderspace__L7_1_1UIrequest.html", "classSpyderspace__L7_1_1UIrequest" ]
    ] ]
];