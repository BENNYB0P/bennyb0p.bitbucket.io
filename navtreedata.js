/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Ben Presley - Cal Poly Portfolio", "index.html", [
    [ "ME 305 Documentation", "305_library.html", [
      [ "Fibonacci Generator", "305_library.html#sec_fib", null ],
      [ "Elevator Simulation", "305_library.html#sec_elevator", null ],
      [ "LED Controller", "305_library.html#sec_blinker", null ],
      [ "Encoder Processing", "305_library.html#sec_Encoder", null ],
      [ "Serial Handshake", "305_library.html#sec_Lab4", null ],
      [ "Bluetooth Connectivity", "305_library.html#sec_Lab5", null ],
      [ "Motor Speed Controller", "305_library.html#sec_Lab6", null ],
      [ "Syncronous Reference Profile", "305_library.html#sec_Lab7", null ]
    ] ],
    [ "ME 405 Documentation", "405_library.html", [
      [ "Change Calculator Function", "405_library.html#sec_405_HW1", null ],
      [ "Vending Machine", "405_library.html#sec_405_Lab1", null ],
      [ "External Interrupt Response Timer", "405_library.html#sec_405_Lab2", null ],
      [ "Lab 3 Serial Connection Interface", "405_library.html#sec_405_Lab3", null ],
      [ "Lab 4 Temperature Sensor i2c Lab", "405_library.html#sec_405_Lab4", null ]
    ] ],
    [ "Term Project - Foundations", "405_term_prj.html", [
      [ "Hand Calculations", "405_term_prj.html#hand_calcs", null ],
      [ "Project Simulation", "405_term_prj.html#prj_sim", null ],
      [ "Touchscreen Driver Script", "405_term_prj.html#prj_touchscreen", null ],
      [ "Motor and Encoder Drivers", "405_term_prj.html#prj_enotor", null ]
    ] ],
    [ "Term Project - Testing", "405_term_prj_testing.html", [
      [ "Term Project Testing", "405_term_prj_testing.html#prj_balance", null ]
    ] ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"",
"classSC__TouchScreen_1_1TouchScreen.html#ab31432cf3b098f955e0d985afe83160d",
"classSpyderspace__L7_1_1UIrequest.html#aa4e67f61854ace146d3f3819d6861fa6"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';