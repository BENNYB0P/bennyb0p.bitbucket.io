var classSpyderspace__L7_1_1UIrequest =
[
    [ "__init__", "classSpyderspace__L7_1_1UIrequest.html#a98bce65b982c6654b372c03b1a07f2da", null ],
    [ "PlotResults", "classSpyderspace__L7_1_1UIrequest.html#af1cce8bda71d538c6a30e87dcfe17315", null ],
    [ "readLine", "classSpyderspace__L7_1_1UIrequest.html#abe304a834754c76d0915597c59357777", null ],
    [ "run", "classSpyderspace__L7_1_1UIrequest.html#a9e10b3a9c384b488cc58e144e9a480cf", null ],
    [ "sendLine", "classSpyderspace__L7_1_1UIrequest.html#aa4e67f61854ace146d3f3819d6861fa6", null ],
    [ "transitionTo", "classSpyderspace__L7_1_1UIrequest.html#a63e770f5fa187ea327a57f79458322f5", null ],
    [ "AlreadyRecorded", "classSpyderspace__L7_1_1UIrequest.html#adbe6c4eef8bc969173d320af5e987d1c", null ],
    [ "blank", "classSpyderspace__L7_1_1UIrequest.html#a0eceb1522b95c3b71d2d25e9f8b3c1c8", null ],
    [ "checkgo", "classSpyderspace__L7_1_1UIrequest.html#a7deb4c5c412efc2db0ad975fa04a9281", null ],
    [ "cleanread", "classSpyderspace__L7_1_1UIrequest.html#a2e5ab9086c1b8737b5f0f5b540e9b70f", null ],
    [ "cleanstring", "classSpyderspace__L7_1_1UIrequest.html#abe44ad7fd220d34c4c509a9948be25f3", null ],
    [ "curr_time", "classSpyderspace__L7_1_1UIrequest.html#a13ecda3d91f39db9efa67c721f2019a4", null ],
    [ "deg_plot", "classSpyderspace__L7_1_1UIrequest.html#a21109e41f0c72613251b659e53c77d31", null ],
    [ "IndexTime", "classSpyderspace__L7_1_1UIrequest.html#a7832cc147faaa1e81bb452315b1efe3b", null ],
    [ "inputKi", "classSpyderspace__L7_1_1UIrequest.html#a9ff646bd347e7a906aa8aceb4563e9ba", null ],
    [ "inputKp", "classSpyderspace__L7_1_1UIrequest.html#a8fb04cd6c96fc5e434eb788ea5af13b2", null ],
    [ "inputPWM", "classSpyderspace__L7_1_1UIrequest.html#ae4cc91f25db5fbe7a4fd885160de1cc2", null ],
    [ "inputSpeed", "classSpyderspace__L7_1_1UIrequest.html#abd1e86ee0986682984202d395ba2a7d3", null ],
    [ "Kpcmd", "classSpyderspace__L7_1_1UIrequest.html#ae34ebf9e630d167744168e9bb411c222", null ],
    [ "line_list", "classSpyderspace__L7_1_1UIrequest.html#af1c0c6c1af4e3bfe7fabffaba1ea9087", null ],
    [ "next_time", "classSpyderspace__L7_1_1UIrequest.html#ab35d2dd4bf66fc2e7241d5b9b1b7d554", null ],
    [ "oldout", "classSpyderspace__L7_1_1UIrequest.html#a419a1b9ef10ca4ac90ca0d1db71dbca2", null ],
    [ "out", "classSpyderspace__L7_1_1UIrequest.html#aa4435374afba718a153d3e4d14292cc5", null ],
    [ "ProfileStartTime", "classSpyderspace__L7_1_1UIrequest.html#a48095ad7c914bc06b5c3cd6de25a598a", null ],
    [ "Rate", "classSpyderspace__L7_1_1UIrequest.html#afa741c1bc23f5e46b244782e7a578e20", null ],
    [ "rawread", "classSpyderspace__L7_1_1UIrequest.html#a4ef615ceca6d5b526b4e2c8c2cfa6c0e", null ],
    [ "Recording", "classSpyderspace__L7_1_1UIrequest.html#a97f81fdc593295a40485859adf35c114", null ],
    [ "rpm_plot", "classSpyderspace__L7_1_1UIrequest.html#a997e8c3cd5a796fad46598a055eefc5f", null ],
    [ "sendit", "classSpyderspace__L7_1_1UIrequest.html#a5d37fc7019d9603215d44c8647c7609d", null ],
    [ "sendKp", "classSpyderspace__L7_1_1UIrequest.html#a27c7288111880e4fc8ab2fbfda0a9d55", null ],
    [ "start_time", "classSpyderspace__L7_1_1UIrequest.html#a1256ec11db9cebad3e5e8570c43c2cfb", null ],
    [ "state", "classSpyderspace__L7_1_1UIrequest.html#add1c27884a728394ce95f2359b46c9c0", null ],
    [ "Syncing", "classSpyderspace__L7_1_1UIrequest.html#aeea70edc736b11b9dedda7d0a764625a", null ],
    [ "Target", "classSpyderspace__L7_1_1UIrequest.html#a02c10f4f8b43f57460ef48aa916891a9", null ],
    [ "time_plot", "classSpyderspace__L7_1_1UIrequest.html#a62b99e18d2bf06d047e443278d1c94e9", null ]
];