var classSC__closedloop__L7_1_1ClosedLoop =
[
    [ "__init__", "classSC__closedloop__L7_1_1ClosedLoop.html#ad0105fef9f5fb220565b80d2ce799eb2", null ],
    [ "clamp", "classSC__closedloop__L7_1_1ClosedLoop.html#a5a5ed62265dfa0b0739d98881fdb70c2", null ],
    [ "Jcalcs", "classSC__closedloop__L7_1_1ClosedLoop.html#a1df582105deb350a5fba97e747b298ac", null ],
    [ "PIcontrol", "classSC__closedloop__L7_1_1ClosedLoop.html#a72c61ea8f94658a5e6d7dbacbfbf625b", null ],
    [ "count", "classSC__closedloop__L7_1_1ClosedLoop.html#a052646e0d5beeb07d083b2853f2c062a", null ],
    [ "duty", "classSC__closedloop__L7_1_1ClosedLoop.html#a0377aa608ebfeea06dec7be14be3e611", null ],
    [ "ErrorI", "classSC__closedloop__L7_1_1ClosedLoop.html#ab56821903762199bd8627ea384c10e18", null ],
    [ "ErrorP", "classSC__closedloop__L7_1_1ClosedLoop.html#ade2e383bca1140e8fe34605650a5359d", null ],
    [ "Jval", "classSC__closedloop__L7_1_1ClosedLoop.html#a974d32e0867bab102794f6d42b801741", null ],
    [ "Jvali", "classSC__closedloop__L7_1_1ClosedLoop.html#ac12bde33091466281249440e0b5a6b30", null ],
    [ "Ki", "classSC__closedloop__L7_1_1ClosedLoop.html#ae1a496179eb144f6b55f5324eebcca89", null ],
    [ "Kp", "classSC__closedloop__L7_1_1ClosedLoop.html#ae910a3d706b694df6f678f020edc5d71", null ],
    [ "myEncoder", "classSC__closedloop__L7_1_1ClosedLoop.html#a48c2dd7377b2d62a5a876464b6bb365c", null ],
    [ "myMotor", "classSC__closedloop__L7_1_1ClosedLoop.html#ac9769c7d76c6ed5d7153f4fa612fd205", null ],
    [ "myuart", "classSC__closedloop__L7_1_1ClosedLoop.html#aee5577617f13dd327bde5c739fe13f00", null ],
    [ "P_running", "classSC__closedloop__L7_1_1ClosedLoop.html#af4f01518473d1a88186bb92580611430", null ],
    [ "proposedDuty", "classSC__closedloop__L7_1_1ClosedLoop.html#ae7d924f2b95fd59cc3a299ac642cb3f8", null ],
    [ "PWM_max", "classSC__closedloop__L7_1_1ClosedLoop.html#a0a374d9dfdc6e710f8cc14177f0d809b", null ],
    [ "PWM_min", "classSC__closedloop__L7_1_1ClosedLoop.html#a7d4848ed72a1b50824d964be98c9b271", null ]
];